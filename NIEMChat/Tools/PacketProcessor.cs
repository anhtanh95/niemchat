﻿using System;

namespace NIEMChat.Tools
{
    public class PacketProcessor
    {
        /// <summary>
        /// Packet's Header Size
        /// </summary>
        public static int HeaderSize = 5;

        /// <summary>
        /// Waiting state
        /// </summary>
        private bool IsWaiting { get; set; }

        /// <summary>
        /// Data buffer
        /// </summary>
        private byte[] DataBuffer { get; set; }

        /// <summary>
        /// Amount of data to wait on
        /// </summary>
        private int WaitForData { get; set; }

        /// <summary>
        /// Callback for when a packet is finished
        /// </summary>
        public delegate void CallPacketFinished(byte[] packet);

        /// <summary>
        /// Event called when a packet has been handled
        /// </summary>
        public event CallPacketFinished PacketFinished;

        /// <summary>
        /// Creates a new instance of <see cref="PacketProcessor"/>
        /// </summary>
        public PacketProcessor()
        {
            DataBuffer = new byte[0x100];
            WaitForData = 0;
            IsWaiting = true;
        }

        /// <summary>
        /// Adds data to the buffer
        /// </summary>
        public void AddData(byte[] data, int offset, int length)
        {
            if (IsWaiting)//đang chưa xử lý packet nào, vẫn ngồi đợi
            {
                //vì chưa xử lý packet nào, nên vào được đến đây, là ta đang ở header của packet
                if (length <= HeaderSize)//header của packet chiếm 2 byte, vậy nếu length của packet <= 2 nghĩa là packet rỗng data hoặc packet lỗi
                {
                    return;
                }
                PacketReader pr = new PacketReader(data);
                int packetLength = pr.ReadInt();//ta lấy packetLength không bao gồm header ra từ packet nhận được;
                DataBuffer = new byte[packetLength + HeaderSize];//Đặt độ dài mới cho buffer tương ứng với độ dài của packet được gửi đến
                Buffer.BlockCopy(data, 0, DataBuffer, 0, length);//add data có trong packet hiện tại vào buffer.
                if (length < packetLength + HeaderSize)//nếu độ dài packet đang đọc được < đồ dài thật sự, thì packet gửi thiếu data, đợi phần còn lại đến
                {
                    WaitForData = packetLength + HeaderSize - length;//lượng data còn lại đang chờ gửi đến tiếp
                    IsWaiting = false;//đặt cho trạng thái hiện tại đang đợi packet mới là false, giờ chờ phần còn lại của packet
                }
                else //nếu đã nhận đầy đủ dử liệu, thì kết thúc quá trình xử lý
                {
                    if (PacketFinished != null)
                        PacketFinished(DataBuffer);
                }
            }
            else //đang xử lý dở 1 packet
            {
                Buffer.BlockCopy(data, 0, DataBuffer, DataBuffer.Length - WaitForData, length);//add data có trong packet hiện tại vào buffer.
                WaitForData -= length;//giảm lượng data cần đợi xuống
                if (WaitForData == 0)//đã nhận hết data của packet, kết thúc quá trình xử lý
                {
                    if (PacketFinished != null)
                        PacketFinished(DataBuffer);
                    IsWaiting = true;//đặt lại trạng thái đang đợi về true
                }
            }
        }
    }
}
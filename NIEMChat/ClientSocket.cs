﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using NIEMChat.Tools;
using System.Drawing;

namespace NIEMChat
{
    public class ClientSocket : IDisposable
    {
        private readonly Socket socket;
        private readonly byte[] socketbuffer;
        private readonly string host;
        private readonly int port;
        private readonly object dispose_Sync;
        private bool disposed;
        public string NickName;
        public TabPage ChatTab;
        public KeyValuePair<string, string> Partner; 

        public PacketProcessor pProcessor { get; private set; }

        public bool Connected
        {
            get
            {
                return disposed == false;
            }
        }

        public string Host
        {
            get
            {
                return host;
            }
        }

        public int Port
        {
            get
            {
                return port;
            }
        }

        public ClientSocket(Socket sock)
        {
            socket = sock;
            socketbuffer = new byte[1024];

            host = ((IPEndPoint)socket.RemoteEndPoint).Address.ToString();
            port = ((IPEndPoint)socket.RemoteEndPoint).Port;
            NickName = "NIEM User";

            dispose_Sync = new object();


            pProcessor = new PacketProcessor();
            pProcessor.PacketFinished += (data) =>
            {
                RecvPacket(data);
            };

            WaitForData();
        }

        private void WaitForData()
        {
            if (!disposed)
            {
                SocketError error = SocketError.Success;
                socket.BeginReceive(socketbuffer, 0, socketbuffer.Length, SocketFlags.None, out error, OnPacketReceived, null);

                if (error != SocketError.Success)
                {
                    Disconnect();
                }
            }
        }

        private void OnPacketReceived(IAsyncResult iar)
        {
            if (!disposed)
            {
                SocketError error = SocketError.Success;
                int size = socket.EndReceive(iar, out error);

                if (size == 0 || error != SocketError.Success)
                {
                    Disconnect();
                }
                else
                {
                    pProcessor.AddData(socketbuffer, 0, size);
                    WaitForData();
                }
            }
        }

        public void Disconnect()
        {
            Dispose();
        }

        public void Dispose()
        {
            lock (dispose_Sync)
            {
                if (disposed == true)
                    return;

                disposed = true;

                try
                {
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                }
                finally
                {
                    //ClientForm.Instance.EnableChat(false);

                    ClientForm.Instance.UserList = new Dictionary<string, string>();
                    Console.WriteLine("Bạn đã bị ngắt kết nối với server.");
                    /*try
                    {
                        Console.WriteLine(string.Format("{0}:{1} Disconnected", SClient.Host, SClient.Port));
                        Program.Users.Remove(IPEndPoint.ToString());
                        MainForm.Instance.RemoveClient(this);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("ERROR: Error when disconneting client. " + e.ToString());
                    }*/
                }
            }
        }

        ~ClientSocket()
        {
            Dispose();
        }

        public void SendData(SendHeader dataType, byte[] pData)
        {
            byte[] data = new byte[pData.Length + PacketProcessor.HeaderSize];
            PacketWriter pw = new PacketWriter();
            pw.WriteInt(pData.Length);
            pw.WriteByte((byte)dataType);
            Buffer.BlockCopy(pw.ToArray(), 0, data, 0, pw.Length);
            Buffer.BlockCopy(pData, 0, data, PacketProcessor.HeaderSize, pData.Length);
            pData = data;
            try
            {
                SendPacket(pData);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[ERROR] Failed sending: {0}", ex.ToString());
            }
        }

        private void SendPacket(byte[] data)
        {
            if (!disposed)
            {
                int offset = 0;

                while (offset < data.Length)
                {
                    SocketError outError = SocketError.Success;
                    int sent = socket.Send(data, offset, data.Length - offset, SocketFlags.None, out outError);

                    if (sent == 0 || outError != SocketError.Success)
                    {
                        Disconnect();
                        return;
                    }

                    offset += sent;
                }
            }
        }

        internal void RecvPacket(byte[] packet)
        {
            PacketReader pr_raw = new PacketReader(packet);
            int packetLength = pr_raw.ReadInt();
            int dataType = pr_raw.ReadByte();
            byte[] data = new byte[packetLength];
            Buffer.BlockCopy(packet, PacketProcessor.HeaderSize, data, 0, packetLength);
            switch ((RecvHeader)dataType)
            {
                case RecvHeader.LoginResult:
                    {
                        PacketReader pr = new PacketReader(data);
                        bool success = pr.ReadBool();
                        if (success)
                        {
                            NickName = pr.ReadString();
                            if (ClientForm.Instance.textBoxNick.InvokeRequired)
                            {
                                ClientForm.Instance.textBoxNick.Invoke(new MethodInvoker(delegate ()
                                {
                                    ClientForm.Instance.textBoxNick.Text = NickName;
                                }));
                            }
                            if (ClientForm.Instance.login.InvokeRequired)
                            {
                                ClientForm.Instance.login.Invoke(new MethodInvoker(delegate ()
                                {
                                    ClientForm.Instance.login.Close();
                                }));
                            }
                            //Console.WriteLine("Tham gia phòng chat thành công.");
                            //Console.WriteLine("Đang lấy danh sách thành viên...");
                            PacketWriter pw = new PacketWriter();
                            pw.WriteBool(true);//true = get all
                            SendData(SendHeader.UserListRequest, pw.ToArray());
                        }
                        else
                        {
                            Console.WriteLine("Tham gia phòng chat thất bại.");
                        }
                        break;
                    }
                case RecvHeader.UserList:
                    {
                        PacketReader pr = new PacketReader(data);
                        Dictionary<string, string> list = new Dictionary<string, string>();
                        int count = pr.ReadInt();
                        for (int i = 0; i < count; i++)
                        {
                            list.Add(pr.ReadString(), pr.ReadString());
                        }
                        ClientForm.Instance.UserList = list;

                        break;
                    }
                case RecvHeader.Chat:
                    {
                        PacketReader pr = new PacketReader(data);
                        string chat = pr.ReadString();
                        FormChat chatRoom = ClientForm.Instance.chatRoom;
                        if (chatRoom != null)
                        {
                            if (!chatRoom.tabControl1.TabPages.ContainsKey("Test"))
                                chatRoom.CreateChatTab("Test");
                            chatRoom.ShowText(chat);
                        }
                        else
                            Console.WriteLine();
                        break;
                    }
                case RecvHeader.FileTransfer:
                    {
                        PacketReader pw = new PacketReader(data);
                        string fileName = pw.ReadString();
                        string savePath = Path.Combine(Application.StartupPath, fileName);
                        Console.WriteLine("Saving file to " + savePath);
                        
                        try
                        {
                            BinaryWriter writer = new BinaryWriter(File.Open(savePath, FileMode.OpenOrCreate));
                            if (pw.Available > 0)
                                writer.Write(pw.ReadBytes(pw.Available));
                            writer.Close();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("[ERROR] Cannot saving file to " + savePath + ". Exception:" + ex);
                        }
                        Console.WriteLine("Đã nhận 1 tập tin \"" + fileName + "\" từ " + Partner.Value);
                        break;
                    }
                default:
                    return;
            }
        }
    }
}

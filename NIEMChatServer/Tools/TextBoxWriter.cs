﻿using System.IO;
using System.Text;
using System.Windows.Forms;

namespace NIEMChatServer.Tools
{
    public class TextBoxWriter : TextWriter
    {
        TextBox _output = null;

        public TextBoxWriter(TextBox output)
        {
            _output = output;
        }

        public override void Write(char value)
        {
            base.Write(value);
            if (_output.InvokeRequired)
            {
                _output.Invoke(new MethodInvoker(() => Write(value)));
                return;
            }
            try
            {
                _output.AppendText(value.ToString());
            } catch { }
        }

        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
}

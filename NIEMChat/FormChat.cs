﻿using NIEMChat.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NIEMChat
{
    public partial class FormChat : Form
    {
        public FormChat()
        {
            InitializeComponent();
        }

        public void SendTextChat()
        {
            try
            {
                string text = textBoxChat.Text;
                PacketWriter pw = new PacketWriter();
                pw.WriteString(ClientForm.Instance.sClient.NickName + ":" + text);
                ClientForm.Instance.sClient.SendData(SendHeader.Chat, pw.ToArray());
                ShowText("You->" + ClientForm.Instance.sClient.Partner.Value + ": " + text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                textBoxChat.Text = "";
            }
        }

        public void ShowText(string value)
        {
            if (tabControl1.InvokeRequired)
            {
                tabControl1.Invoke(new MethodInvoker(() => ShowText(value)));
                return;
            }
            TabPage selectedTab = tabControl1.SelectedTab;
            TextBox _output = (TextBox)selectedTab.Controls[0];
            if (_output.InvokeRequired)
            {
                _output.Invoke(new MethodInvoker(() => ShowText(value)));
                return;
            }
            try
            {
                _output.AppendText("[" + DateTime.Now + "]" + value + "\r\n");
            }
            catch { }
        }

        private void FormChat_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClientForm.Instance.buttonChatRoom.Enabled = true;
        }

        private void listViewClientChat_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo info = listViewClientChat.HitTest(e.Location);
            ListViewItem item = info.Item;

            if (item != null)
            {
                //Console.WriteLine("Connected to: " + item.SubItems[1].Text + "(" + item.Text + ")");
                PacketWriter pw = new PacketWriter();
                pw.WriteString(item.Text);
                pw.WriteString(item.SubItems[1].Text);
                ClientForm.Instance.sClient.SendData(SendHeader.ConnectPartner, pw.ToArray());
                ClientForm.Instance.sClient.Partner = new KeyValuePair<string, string>(item.Text, item.SubItems[1].Text);

                CreateChatTab(item.SubItems[1].Text);
            }
        }

        public TabPage CreateChatTab(string tabName)
        {
            TabPage page = new TabPage();
            page.Text = tabName;
            page.Name = tabName;

            TextBox chatBox = new TextBox();
            chatBox.Multiline = true;
            chatBox.Size = new Size(438, 316);
            page.Controls.Add(chatBox);
            ClientForm.Instance.sClient.ChatTab = page;

            if (tabControl1.InvokeRequired)
            {
                tabControl1.Invoke(new MethodInvoker(() => CreateChatTab(tabName)));
                return null;
            }
            tabControl1.TabPages.Add(page);
            tabControl1.SelectedTab = page;
            EnableChat(true);
            return page;
        }



        public void EnableChat(bool enable)
        {
            if (textBoxChat.InvokeRequired)
            {
                textBoxChat.Invoke(new MethodInvoker(() => EnableChat(enable)));
                return;
            }
            textBoxChat.Enabled = enable;

            if (buttonSend.InvokeRequired)
            {
                buttonSend.Invoke(new MethodInvoker(() => EnableChat(enable)));
                return;
            }
            buttonSend.Enabled = enable;

            //textBoxFilePath.Enabled = enable;
            //buttonBrowse.Enabled = enable;
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            SendTextChat();
        }

        private void textBoxChat_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendTextChat();
            }
        }
    }
}

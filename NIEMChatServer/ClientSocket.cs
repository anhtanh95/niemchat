﻿using NIEMChatServer.Tools;
using System;
using System.Net;
using System.Net.Sockets;

namespace NIEMChatServer
{
    public class ClientSocket : IDisposable
    {
        private readonly Socket socket;
        private readonly byte[] socketbuffer;
        private readonly string host;
        private readonly int port;
        private readonly object dispose_Sync;
        private readonly Client client;
        private bool disposed;

        public PacketProcessor pProcessor { get; private set; }

        public bool Connected
        {
            get
            {
                return disposed == false;
            }
        }

        public string Host
        {
            get
            {
                return host;
            }
        }

        public int Port
        {
            get
            {
                return port;
            }
        }

        public IPEndPoint IPEndPoint
        {
            get
            {
                return (IPEndPoint)socket.RemoteEndPoint;
            }
        }

        public ClientSocket(Client pClient, Socket sock)
        {
            client = pClient;
            socket = sock;
            socketbuffer = new byte[1024];

            host = ((IPEndPoint)socket.RemoteEndPoint).Address.ToString();
            port = ((IPEndPoint)socket.RemoteEndPoint).Port;

            dispose_Sync = new object();


            pProcessor = new PacketProcessor();
            pProcessor.PacketFinished += (data) =>
            {
                pClient.RecvPacket(data);
            };

            WaitForData();
        }

        private void WaitForData()
        {
            if (!disposed)
            {
                SocketError error = SocketError.Success;
                socket.BeginReceive(socketbuffer, 0, socketbuffer.Length, SocketFlags.None, out error, OnPacketReceived, null);

                if (error != SocketError.Success)
                {
                    Disconnect();
                }
            }
        }

        private void OnPacketReceived(IAsyncResult iar)
        {
            if (!disposed)
            {
                SocketError error = SocketError.Success;
                int size = socket.EndReceive(iar, out error);

                if (size == 0 || error != SocketError.Success)
                {
                    Disconnect();
                }
                else
                {
                    pProcessor.AddData(socketbuffer, 0, size);
                    WaitForData();
                }
            }
        }

        public void Disconnect()
        {
            Dispose();
        }

        public void Dispose()
        {
            lock (dispose_Sync)
            {
                if (disposed == true)
                    return;

                disposed = true;

                try
                {
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                }
                finally
                {
                    client.Disconnected();
                }
            }
        }

        ~ClientSocket()
        {
            Dispose();
        }

        public void SendData(SendHeader dataType, byte[] pData)
        {
            byte[] data = new byte[pData.Length + PacketProcessor.HeaderSize];
            PacketWriter pw = new PacketWriter();
            pw.WriteInt(pData.Length);
            pw.WriteByte((byte)dataType);
            Buffer.BlockCopy(pw.ToArray(), 0, data, 0, pw.Length);
            Buffer.BlockCopy(pData, 0, data, PacketProcessor.HeaderSize, pData.Length);
            pData = data;
            try
            {
                SendPacket(pData);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[ERROR] Failed sending: {0}", ex.ToString());
            }
        }

        public void SendPacket(byte[] final)
        {
            if (!disposed)
            {
                int offset = 0;

                while (offset < final.Length)
                {
                    SocketError outError = SocketError.Success;
                    int sent = socket.Send(final, offset, final.Length - offset, SocketFlags.None, out outError);

                    if (sent == 0 || outError != SocketError.Success)
                    {
                        Disconnect();
                        return;
                    }

                    offset += sent;
                }
            }
        }
    }
}

﻿namespace NIEMChat.Tools
{
    public enum SendHeader : ushort
    {
        Login = 0,
        UserListRequest = 1,
        ConnectPartner = 2,
        Chat = 3,
        FileTransfer = 4,
    }
}

﻿using System;
using System.Windows.Forms;

namespace NIEMChat
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ClientForm());
        }
        
        public static string ServerIP = "127.0.0.1";
        public static int ServerPort = 8486;
    }
}

﻿namespace NIEMChat.Tools
{
    public enum RecvHeader : ushort
    {
        LoginResult = 0,
        UserList = 1,
        Chat = 2,
        FileTransfer = 3,
    }
}

﻿namespace NIEMChatServer.Tools
{
    public enum SendHeader : ushort
    {
        LoginResult = 0,
        UserList = 1,
        Chat = 2,
        FileTransfer = 3,
    }
}

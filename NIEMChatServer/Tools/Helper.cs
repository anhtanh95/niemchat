﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace NIEMChatServer.Tools
{
    public class Helper
    {
        public static string GetSHA1HashString(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();
            byte[] hashBytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
            StringBuilder sb = new StringBuilder();
            foreach (byte b in hashBytes)
            {
                sb.Append(b.ToString("X2"));
            }
            return sb.ToString();
        }
    }
}

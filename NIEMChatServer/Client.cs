﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using NIEMChatServer.Tools;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace NIEMChatServer
{
    public class Client
    {
        public ClientSocket SClient { get; set; }
        public bool Connected { get; set; }
        public string NickName;
        public KeyValuePair<string, string> Partner;

        public IPEndPoint IPEndPoint { get; set; }

        public Client(Socket session)
        {
            SClient = new ClientSocket(this, session);
            IPEndPoint = SClient.IPEndPoint;
            NickName = "NIEM User";
            Console.WriteLine(string.Format("{0}:{1} Connnected", SClient.Host, SClient.Port));
            Connected = true;
        }

        public void Disconnect(string reason, params object[] values)
        {
            Console.WriteLine("Disconnected client with reason: " + string.Format(reason, values));
            if (SClient != null)
                SClient.Disconnect();
        }

        internal void Disconnected()
        {
            try
            {
                if (Connected)
                {
                    Console.WriteLine(string.Format("{0}:{1} Disconnected", SClient.Host, SClient.Port));
                }
                Connected = false;
                Program.Users.Remove(IPEndPoint.ToString());
                ServerForm.Instance.RemoveClient(this);
                ServerForm.Instance.UpdateUserList();
                SClient.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: Error when disconneting client. " + e.ToString());
            }
        }
        
        internal void RecvPacket(byte[] rawData)
        {
            PacketReader pr_raw = new PacketReader(rawData);
            int packetLength = pr_raw.ReadInt();
            int dataType = pr_raw.ReadByte();
            byte[] data = new byte[packetLength];
            Buffer.BlockCopy(rawData, PacketProcessor.HeaderSize, data, 0, packetLength);
            switch ((RecvHeader)dataType)
            {
                case RecvHeader.Login:
                    {
                        PacketWriter pw;
                        PacketReader pr = new PacketReader(data);
                        //NickName 
                        string user = pr.ReadString();
                        string password = pr.ReadString();
                        bool loginStt = false;
                        string hashString = Helper.GetSHA1HashString(password);

                        MySqlDataReader dr = null;
                        try
                        {
                            Program.MySqlCon.Open();
                            string sql = "SELECT * FROM user where user='" + user + "' AND password='" + hashString + "'";
                            MySqlCommand cmd = new MySqlCommand(sql, Program.MySqlCon);
                            dr = cmd.ExecuteReader();
                            if (dr.HasRows)
                            {
                                dr.Read();
                                loginStt = true;
                                NickName = dr.GetString(4);
                                ServerForm.Instance.AddClient(this);
                                Program.Users.Add(IPEndPoint.ToString(), this);

                                //Send server message to client
                                pw = new PacketWriter();
                                pw.WriteString("[Server Message]:" + ServerForm.Instance.ServerMessage);
                                SClient.SendData(SendHeader.Chat, pw.ToArray());
                            }
                        }
                        catch (Exception e)
                        {
                            Disconnect(e.ToString());
                            loginStt = false;
                        }
                        finally
                        {
                            if (Program.MySqlCon != null)
                            {
                                Program.MySqlCon.Close();
                            }
                            if (dr != null)
                            {
                                dr.Close();
                            }
                        }
                        pw = new PacketWriter();
                        pw.WriteBool(loginStt);
                        if (loginStt)
                        {
                            pw.WriteString(NickName);
                        }
                        SClient.SendData(SendHeader.LoginResult, pw.ToArray());
                        break;
                    }
                case RecvHeader.UserListRequest:
                    {
                        ServerForm.Instance.UpdateUserList();
                        /*PacketWriter pw = new PacketWriter();
                        pw.WriteInt(Program.Users.Count);
                        foreach (KeyValuePair<string, Client> pair in Program.Users)
                        {
                            pw.WriteString(pair.Key);
                            pw.WriteString(pair.Key.Equals(SClient.IPEndPoint.ToString()) ? "*You*" :  pair.Value.NickName);
                        }
                        SClient.SendData(SendHeader.UserList, pw.ToArray());*/
                        break;
                    }
                case RecvHeader.ConnectPartner:
                    {
                        PacketReader pr = new PacketReader(data);
                        Partner = new KeyValuePair<string, string>(pr.ReadString(), pr.ReadString());
                        Console.WriteLine(SClient.IPEndPoint.ToString() + " connect to " + Partner.Key);
                        break;
                    }
                case RecvHeader.Chat:
                case RecvHeader.FileTransfer:
                    {
                        PacketWriter pw = new PacketWriter();
                        pw.WriteBytes(data);
                        Client partnerClient = Program.Users[Partner.Key];
                        if (partnerClient != null)
                        {
                            partnerClient.SClient.SendData((RecvHeader)dataType == RecvHeader.Chat ? SendHeader.Chat : SendHeader.FileTransfer, pw.ToArray());
                        }
                        break;
                    }
                default:
                    return;
            }
        }
    }
}

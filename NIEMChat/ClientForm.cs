﻿using NIEMChat.Tools;
using System;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Collections.Generic;
using System.IO;

namespace NIEMChat
{
    public partial class ClientForm : Form
    {
        public static ClientForm Instance;

        public ClientSocket sClient;
        private Dictionary<string, string> userList;
        public Dictionary<string, string> UserList
        {
            get
            {
                return userList;
            }
            set
            {
                ClientForm.Instance.UpdateAllUser(listViewClient, value);
                if (ClientForm.Instance.chatRoom != null)
                    ClientForm.Instance.UpdateAllUser(ClientForm.Instance.chatRoom.listViewClientChat, value);
                userList = value;
            }
        }

        public FormChat chatRoom;

        public ClientForm()
        {
            Instance = this;
            StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }

        public void Connect(string username, string password)
        {
            if (sClient != null && sClient.Connected)
            {
                sClient.Disconnect();
                textBoxNick.Enabled = true;
                return;
            }
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                //Kết nối tới server. 
                socket.Connect(Program.ServerIP, Program.ServerPort);
                Console.WriteLine("Đang kết nối tới server...");
            }
            catch (SocketException ex)
            {
                Console.WriteLine("Không thể kết nối đến server:" + ex.Message, "Error!");
                return;
            }
            sClient = new ClientSocket(socket);
            Console.WriteLine("Kết nối thành công.");
            textBoxNick.Enabled = false;
            sClient.NickName = textBoxNick.Text;

            //Gửi thông báo login đến server

            PacketWriter pw = new PacketWriter();
            pw.WriteString(username);
            pw.WriteString(password);
            sClient.SendData(SendHeader.Login, pw.ToArray());
            //Console.WriteLine("Đang tham gia vào phòng chat...");
        }

        private void ClientForm_Load(object sender, EventArgs e)
        {
            Console.SetOut(new TextBoxWriter(textBoxMain));
            Console.WriteLine("Chào mừng bạn đến với NIEM Chat!");
        }

        //private void buttonSend_Click(object sender, EventArgs e)
        //{
        //    SendTextChat();
        //}

        //private void textBoxChat_KeyUp(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        SendTextChat();
        //    }
        //}

        //private void listViewClient_MouseDoubleClick(object sender, MouseEventArgs e)
        //{
        //    ListViewHitTestInfo info = listViewClient.HitTest(e.Location);
        //    ListViewItem item = info.Item;

        //    if (item != null)
        //    {
        //        Console.WriteLine("Connected to: " + item.SubItems[1].Text + "(" + item.Text + ")");
        //        PacketWriter pw = new PacketWriter();
        //        pw.WriteString(item.Text);
        //        pw.WriteString(item.SubItems[1].Text);
        //        sClient.SendData(SendHeader.ConnectPartner, pw.ToArray());
        //        sClient.Partner = new KeyValuePair<string, string>(item.Text, item.SubItems[1].Text);
        //        EnableChat(true);
        //    }
        //}

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog();
        }

        private void buttonFileTransfer_Click(object sender, EventArgs e)
        {
            if (!File.Exists(textBoxFilePath.Text))
            {
                Console.WriteLine("[ERROR] File not found!");
                return;
            }
            string fileName = Path.GetFileName(textBoxFilePath.Text);
            byte[] fileData = File.ReadAllBytes(textBoxFilePath.Text);
            PacketWriter pw = new PacketWriter();
            pw.WriteString(fileName);
            if (fileData.Length > 0)
                pw.WriteBytes(fileData);
            sClient.SendData(SendHeader.FileTransfer, pw.ToArray());
            Console.WriteLine("Đã gửi 1 tệp tin \"" + fileName + "\" đến " + sClient.Partner.Value);
        }

        private void textBoxFilePath_MouseClick(object sender, MouseEventArgs e)
        {
            OpenFileDialog();
        }

        //public void EnableChat(bool enable)
        //{
        //    if (textBoxChat.InvokeRequired)
        //    {
        //        textBoxChat.Invoke(new MethodInvoker(() => EnableChat(enable)));
        //        return;
        //    }
        //    textBoxChat.Enabled = enable;

        //    if (buttonSend.InvokeRequired)
        //    {
        //        buttonSend.Invoke(new MethodInvoker(() => EnableChat(enable)));
        //        return;
        //    }
        //    buttonSend.Enabled = enable;

        //    textBoxFilePath.Enabled = enable;
        //    buttonBrowse.Enabled = enable;
        //}

        public void UpdateUser(string key, string value)
        {
            if (listViewClient.InvokeRequired)
            {
                listViewClient.Invoke(new MethodInvoker(() => UpdateUser(key, value)));
                return;
            }

            ListViewItem lvi = new ListViewItem(key);//ip address
            lvi.SubItems.Add(value);//nick name
            listViewClient.Items.Add(lvi);
        }

        public void UpdateAllUser(ListView listView, Dictionary<string, string> list)
        {
            if (listView.InvokeRequired)
            {
                listView.Invoke(new MethodInvoker(() => UpdateAllUser(listView, list)));
                return;
            }

            listView.Items.Clear();
            foreach (KeyValuePair<string, string> pair in list)
            {
                ListViewItem lvi = new ListViewItem(pair.Key);//ip address
                lvi.SubItems.Add(pair.Value);//nick name
                listView.Items.Add(lvi);
            }
        }

        public void RemoveUser(string key)
        {
            if (listViewClient.InvokeRequired)
            {
                listViewClient.Invoke(new MethodInvoker(() => RemoveUser(key)));
                return;
            }

            listViewClient.Items.Remove(listViewClient.FindItemWithText(key));
        }

        //public void SendTextChat()
        //{
        //    try
        //    {
        //        string text = textBoxChat.Text;
        //        PacketWriter pw = new PacketWriter();
        //        pw.WriteString(sClient.NickName + ":" + text);
        //        sClient.SendData(SendHeader.Chat, pw.ToArray());
        //        Console.WriteLine("You->" + sClient.Partner.Value + ": " + text);
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //    }
        //    finally
        //    {
        //        textBoxChat.Text = "";
        //    }
        //}

        private void OpenFileDialog()
        {
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            // Process input if the user clicked OK.
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxFilePath.Text = openFileDialog1.FileName;
                buttonFileTransfer.Enabled = true;
            }
        }
        public LoginForm login;
        private void ClientForm_Shown(object sender, EventArgs e)
        {
            login = new LoginForm();
            login.StartPosition = FormStartPosition.CenterParent;
            login.ShowDialog(this);
        }

        private void buttonChatRoom_Click(object sender, EventArgs e)
        {
            chatRoom = new FormChat();
            chatRoom.Show();
            UpdateAllUser(chatRoom.listViewClientChat, userList);
            buttonChatRoom.Enabled = false;
        }

        private void buttonEditNick_Click(object sender, EventArgs e)
        {
            if (buttonEditNick.Text == "Save")
            {
                buttonEditNick.Text = "Edit";
                textBoxNick.Enabled = false;
                //Send packet update NickName to client
            }
            else
            {
                buttonEditNick.Text = "Save";
                textBoxNick.Enabled = true;
            }
        }

        private void textBoxNick_TextChanged(object sender, EventArgs e)
        {
            if (textBoxNick.Text != sClient.NickName)
            {
                buttonEditNick.Text = "Save";
            }
        }
    }
}

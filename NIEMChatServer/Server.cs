﻿using System;
using System.Net;
using System.Net.Sockets;

namespace NIEMChatServer
{
    public class Server
    {
        private bool isStarted = false;
        public bool Started
        {
            get
            {
                return isStarted;
            }
        }
        public delegate void ClientConnectedHandler(Socket client);
        public event ClientConnectedHandler OnClientConnected;

        TcpListener Listener;
        public void Start(IPAddress ip, int port)
        {
            Listener = new TcpListener(ip, port);
            Listener.Start(50);
            Console.WriteLine("Đang lắng nghe trên port:" + port);
            Listener.BeginAcceptSocket(OnNewClientConnect, null);
            isStarted = true;
        }

        private void OnNewClientConnect(IAsyncResult iar)
        {
            Socket clientsocket;
            try
            {
                clientsocket = Listener.EndAcceptSocket(iar);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("EndAccept Server Error: {0}", e));
                return;
            }

            try
            {
                Listener.BeginAcceptSocket(OnNewClientConnect, null);
                if (OnClientConnected != null)
                {
                    OnClientConnected(clientsocket);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("Server Error: {0}", ex));
            }
        }
    }
}

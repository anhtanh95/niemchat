﻿using NIEMChatServer.Tools;
using System;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Collections.Generic;

namespace NIEMChatServer
{
    public partial class ServerForm : Form
    {
        public static ServerForm Instance;
        Server server;
        public string ServerMessage = "Xin chào bạn! Tại sao không click vào phòng chat để tiếp tục trò chuyện cùng bạn bè?";

        public ServerForm()
        {
            InitializeComponent();
            Instance = this;
            textBox2.Text = ServerMessage;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Console.SetOut(new TextBoxWriter(textBoxMainSv));
            Console.WriteLine("Chào mừng bạn đến với NIEM Chat Server!");
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (server != null && server.Started)
            {
                buttonStart.Text = "Start Server";
            }
            server = new Server();
            server.OnClientConnected += ClientConnect;
            server.Start(IPAddress.Any, int.Parse(textBox1.Text));
            buttonStart.Text = "Shutdown";
        }

        private static void ClientConnect(Socket client)
        {
            Client Client = new Client(client);
        }

        public void AddClient(Client client)
        {
            if (listViewClient.InvokeRequired)
            {
                listViewClient.Invoke(new MethodInvoker(() => AddClient(client)));
                return;
            }

            ListViewItem lvi = new ListViewItem(client.SClient.Host + ":" + client.SClient.Port);//ip address
            lvi.SubItems.Add(client.NickName);//nick name
            listViewClient.Items.Add(lvi);
        }

        public void RemoveClient(Client client)
        {
            if (listViewClient.InvokeRequired)
            {
                listViewClient.Invoke(new MethodInvoker(() => RemoveClient(client)));
                return;
            }

            listViewClient.Items.Remove(listViewClient.FindItemWithText(client.SClient.Host + ":" + client.SClient.Port));
        }

        public void UpdateUserList()
        {
            foreach (KeyValuePair<string, Client> user in Program.Users)
            {
                PacketWriter pw = new PacketWriter();
                pw.WriteInt(Program.Users.Count);
                foreach (KeyValuePair<string, Client> pair in Program.Users)
                {
                    pw.WriteString(pair.Key);
                    pw.WriteString(pair.Key.Equals(user.Value.SClient.IPEndPoint.ToString()) ? "*You*" : pair.Value.NickName);
                }
                user.Value.SClient.SendData(SendHeader.UserList, pw.ToArray());
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "")
            {
                ServerMessage = textBox2.Text;
                Console.WriteLine("Lời nhắn đã được thay đổi:" + textBox2.Text);
            }
        }
    }
}

﻿namespace NIEMChat
{
    partial class ClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxMain = new System.Windows.Forms.TextBox();
            this.listViewClient = new System.Windows.Forms.ListView();
            this.columnIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnNickName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.textBoxNick = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxChat = new System.Windows.Forms.TextBox();
            this.buttonSend = new System.Windows.Forms.Button();
            this.textBoxFilePath = new System.Windows.Forms.TextBox();
            this.buttonFileTransfer = new System.Windows.Forms.Button();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.buttonChatRoom = new System.Windows.Forms.Button();
            this.buttonEditNick = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::NIEMChat.Properties.Resources.hoc_vien_quan_ly_giao_duc_500x500;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(402, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(75, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxMain
            // 
            this.textBoxMain.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBoxMain.Location = new System.Drawing.Point(6, 47);
            this.textBoxMain.Multiline = true;
            this.textBoxMain.Name = "textBoxMain";
            this.textBoxMain.ReadOnly = true;
            this.textBoxMain.Size = new System.Drawing.Size(332, 214);
            this.textBoxMain.TabIndex = 6;
            // 
            // listViewClient
            // 
            this.listViewClient.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnIP,
            this.columnNickName});
            this.listViewClient.Location = new System.Drawing.Point(344, 93);
            this.listViewClient.Name = "listViewClient";
            this.listViewClient.Size = new System.Drawing.Size(180, 168);
            this.listViewClient.TabIndex = 7;
            this.listViewClient.UseCompatibleStateImageBehavior = false;
            this.listViewClient.View = System.Windows.Forms.View.Details;
            // 
            // columnIP
            // 
            this.columnIP.Text = "IP";
            this.columnIP.Width = 100;
            // 
            // columnNickName
            // 
            this.columnNickName.Text = "Nick Name";
            this.columnNickName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnNickName.Width = 80;
            // 
            // textBoxNick
            // 
            this.textBoxNick.Location = new System.Drawing.Point(72, 6);
            this.textBoxNick.Name = "textBoxNick";
            this.textBoxNick.Size = new System.Drawing.Size(91, 20);
            this.textBoxNick.TabIndex = 8;
            this.textBoxNick.TextChanged += new System.EventHandler(this.textBoxNick_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Nick Name:";
            // 
            // textBoxChat
            // 
            this.textBoxChat.Enabled = false;
            this.textBoxChat.Location = new System.Drawing.Point(6, 267);
            this.textBoxChat.Name = "textBoxChat";
            this.textBoxChat.Size = new System.Drawing.Size(269, 20);
            this.textBoxChat.TabIndex = 10;
            // 
            // buttonSend
            // 
            this.buttonSend.Enabled = false;
            this.buttonSend.Location = new System.Drawing.Point(281, 265);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(57, 23);
            this.buttonSend.TabIndex = 11;
            this.buttonSend.Text = "Send";
            this.buttonSend.UseVisualStyleBackColor = true;
            // 
            // textBoxFilePath
            // 
            this.textBoxFilePath.Enabled = false;
            this.textBoxFilePath.Location = new System.Drawing.Point(6, 293);
            this.textBoxFilePath.Name = "textBoxFilePath";
            this.textBoxFilePath.Size = new System.Drawing.Size(220, 20);
            this.textBoxFilePath.TabIndex = 12;
            this.textBoxFilePath.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBoxFilePath_MouseClick);
            // 
            // buttonFileTransfer
            // 
            this.buttonFileTransfer.Enabled = false;
            this.buttonFileTransfer.Location = new System.Drawing.Point(265, 291);
            this.buttonFileTransfer.Name = "buttonFileTransfer";
            this.buttonFileTransfer.Size = new System.Drawing.Size(73, 23);
            this.buttonFileTransfer.TabIndex = 13;
            this.buttonFileTransfer.Text = "File Transfer";
            this.buttonFileTransfer.UseVisualStyleBackColor = true;
            this.buttonFileTransfer.Click += new System.EventHandler(this.buttonFileTransfer_Click);
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Enabled = false;
            this.buttonBrowse.Location = new System.Drawing.Point(234, 291);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(25, 23);
            this.buttonBrowse.TabIndex = 14;
            this.buttonBrowse.Text = "...";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // buttonChatRoom
            // 
            this.buttonChatRoom.Location = new System.Drawing.Point(344, 275);
            this.buttonChatRoom.Name = "buttonChatRoom";
            this.buttonChatRoom.Size = new System.Drawing.Size(115, 38);
            this.buttonChatRoom.TabIndex = 15;
            this.buttonChatRoom.Text = "Phòng Chat";
            this.buttonChatRoom.UseVisualStyleBackColor = true;
            this.buttonChatRoom.Click += new System.EventHandler(this.buttonChatRoom_Click);
            // 
            // buttonEditNick
            // 
            this.buttonEditNick.Location = new System.Drawing.Point(169, 4);
            this.buttonEditNick.Name = "buttonEditNick";
            this.buttonEditNick.Size = new System.Drawing.Size(75, 23);
            this.buttonEditNick.TabIndex = 16;
            this.buttonEditNick.Text = "Edit";
            this.buttonEditNick.UseVisualStyleBackColor = true;
            this.buttonEditNick.Click += new System.EventHandler(this.buttonEditNick_Click);
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 332);
            this.Controls.Add(this.buttonEditNick);
            this.Controls.Add(this.buttonChatRoom);
            this.Controls.Add(this.buttonBrowse);
            this.Controls.Add(this.buttonFileTransfer);
            this.Controls.Add(this.textBoxFilePath);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.textBoxChat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxNick);
            this.Controls.Add(this.listViewClient);
            this.Controls.Add(this.textBoxMain);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ClientForm";
            this.Text = "NIEM Chat";
            this.Load += new System.EventHandler(this.ClientForm_Load);
            this.Shown += new System.EventHandler(this.ClientForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxMain;
        private System.Windows.Forms.ColumnHeader columnIP;
        private System.Windows.Forms.ColumnHeader columnNickName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxChat;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.TextBox textBoxFilePath;
        private System.Windows.Forms.Button buttonFileTransfer;
        private System.Windows.Forms.Button buttonBrowse;
        public System.Windows.Forms.TextBox textBoxNick;
        private System.Windows.Forms.Button buttonEditNick;
        public System.Windows.Forms.ListView listViewClient;
        public System.Windows.Forms.Button buttonChatRoom;
    }
}


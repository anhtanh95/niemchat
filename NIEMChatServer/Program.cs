﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace NIEMChatServer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            MySqlCon = new MySqlConnection(connectionString);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ServerForm());
        }

        private static string connectionString = "Server=localhost;Port=3306;Database=niemchat;Uid=root;Pwd=root;";
        public static MySqlConnection MySqlCon;
        public static Dictionary<string, Client> Users = new Dictionary<string, Client>();
    }
}
